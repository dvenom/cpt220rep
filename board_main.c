#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "shared.h"

/** enum cell {
   C_NOUGHT = 'O',
   C_CROSS = 'X',
   C_EMPTY = ' ',
   C_INVALID = -1
}; */

/* typedef enum cell game_board[BOARDHEIGHT][BOARDWIDTH]; */

int main(void) {
   enum cell game_board[BOARDHEIGHT][BOARDWIDTH];
   init_board(game_board);
   display_board(game_board);
   return EXIT_SUCCESS;
}
