/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Darrell Thoonen
* Student Number   : s3482232
* Start up code provided by Paul Miller
***********************************************************************/
#include "board.h"

/**
 * @file board.c contains function implementations for managing the @ref
 * game_board
 **/


/**
 * @param board the game board to display
 **/
void init_board(game_board board)
{
   /**
    * this function is used to 
    * set each cell to enum cell empty
    * which give an empty board.
    **/
    printf("Initializng game board\n");

    /**
     * here memset is used to populate the
     * game_board array.
     **/
    memset(*board, C_EMPTY, sizeof(game_board));
}

/**
 * @param board the game board to display
 **/
void display_board(game_board board)
{
   /**
    * iterate over the 2 dimensions of the board array
    * display what each cell contains
    **/

   int height = 0;
   int width = 0;
   printf("-------------------\n");
   printf("|   | A || B || C |\n");
   printf("-------------------\n");
   for (;height<BOARDHEIGHT;++height) {
      printf("| %d ", height+1);
      for (;width<BOARDWIDTH;++width) {
         printf("| %c |", board[height][width]);
      }
      printf("\n-------------------\n");
      width = 0;
   }
}
