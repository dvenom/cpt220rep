/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Darrell Thoonen
* Student Number   : s3482232
* Start up code provided by Paul Miller
***********************************************************************/
#include "helpers.h"

/**
 * @file helpers.c contains all functions used in this program that don't
 * nicely fit into another module. For example, input/output functions and
 * validation functions.
 **/

/**
 * clears the input buffer when there is leftover input in the buffer. 
 * This function should only be called when there is leftover input in the 
 * incoming buffer. Please refer to the fgets manpage (man fgets) on the 
 * university linux servers.
 **/
static void read_rest_of_line(void)
{
        int ch;
        while(ch = getc(stdin), ch != '\n' && ch != EOF)
                ;
        clearerr(stdin);
}

/**
 * helper function to get a human turn
 * @param coord this is the input for a human turn
 **/
/*
int[] get_turn(enum cell token) {
   char y;
   int x;
   char coord [3];
   printf("Enter turn using the format A,1 : ");
   fgets(coord, 3, stdin);
   strtok(coord, ", ");
   y = coord[0];
   x = coord[1];
   printf("raw : %d for y and %d for x\n", y, x);
   printf("processed : %d for y and %d for x\n", coord[0], coord[1]);
   read_rest_of_line(); 
} */
