/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Darrell Thoonen
* Student Number   : s3482232
* Start up code provided by Paul Miller
***********************************************************************/

Please write any comments about your implementation which may be useful for
your marker or a user of your program.

This project is not complete because I had trouble understanding the concepts
of enumerations and structures in C. Having some text book reading may have
helped me understand a bit quicker.

The score board source has hardly been touched. 

There are other bugs in my game as well like but not limited to:
Won't read a players input to place a token.
The main game loop causes the computer to place tokens in all cells.

What it can do:
Randomise token assignment.
Enter a players name, set the human player type and token.
Set the computer players name, type and token.
Initialise the game board to all empty cells.
Display the game board.

The only reason I am submitting an incomplete assignment is basically so I
have something handed in on time.
