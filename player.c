/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Darrell Thoonen
* Student Number   : s3482232
* Start up code provided by Paul Miller
***********************************************************************/
#include "player.h"


/**
 * @file player.c contains functions to manage the @ref player data structure
 * which represents either a @ref HUMAN or @ref COMPUTER player
 **/

/**
 * @param human a pointer to the human player's struct @param token a pointer
 * to where a copy of the human player's token is stored so it can be accessed
 * later @return a result status indicating whether i/o was successful or not
 * or if the user pressed enter or ctrl-d on an empty line.
 **/
enum input_result init_human_player(struct player * human, enum cell * token)
{
   /**
    * 1 assign the player type
    * 2 assign the derefenced player token
    * 3 function call to get player's name
    * 4 return SUCCESS
    **/ 

   /**
    * NOTE: token is passed as a pointer of the type
    * of enum cell. It must be dereferenced before
    * it can be assigned to human->token which is of
    * type enum cell.
    **/

   char human_name[NAMELEN+2];

   /* block to get player name */
   printf("enter name please : ");
  /* gets(human_name); */
   fgets(human_name, NAMELEN, stdin);
   strtok(human_name, "\n");
   /* end block */

   human->type = HUMAN;
   strcpy(human->name, human_name);

   /* DEBUG 
   printf("Human name: %s", human->name);
   printf("Human type: %d\n", human->type);
   printf("Human token: %c\n", human->token); */
        return SUCCESS;
}

/**
 * @param computer a pointer to the computer player struct @param token the
 * token for the human player - the computer one needs to be the opposite
 * @return an indication of i/o success or failure which in this case can be
 * safely ignored as there is no i/o. It is only part of the interface so that
 * human and computer functions are called in a uniform way.
 **/
enum input_result init_computer_player(struct player * computer, enum cell token)
{
   /**
    * assign the computer player to name "Computer"
    * assign the computer player type to COMPUTER
    * assign the computer token to parameter token
    **/
   
   /* struct player * computer_player; */
   char* computer_name = "computer";

   strcpy(computer->name, computer_name); 
   computer->type = COMPUTER;
   computer->token = token;

   /* DEBUG 
   printf("Computer name: %s\n", computer->name);
   printf("Computer type: %d\n", computer->type);
   printf("Computer token: %c\n", computer->token); */
        return SUCCESS;
}

/**
 * @param player the player to display the name of @return a pointer to the
 * player's name
 **/
const char * player_to_string(const struct player* player)
{
        return player->name;
}

/**
 * @param board the gameboard to insert the token into @param player a pointer
 * to the player struct that represents the current player.  @return a value
 * indicating whether any i/o operations were successful
 **/
enum input_result take_turn(game_board board, struct player * player)
{
   /**
    * Gameboard passed in.
    * Pointer to current player passed in.
    * Determine player type.
    * (HUMAN)
    * Ask co-ords to place token
    * (COMP)
    * Randomly pick a cell
    * Check if the cell is empty
    * if) Place token.
    * else) Ask again.
    * Swap player.
    **/
   
   struct player *cur_player;
   int x,y,type;
   char turn [3];
   cur_player = player;
   type = cur_player->type;
   
   /* clear the screen and display the board */
   printf("");
   display_board(board);
   printf("%s's turn\n", player_to_string(cur_player));

   /**
    * if the current player is computer
    * randomly select x and y cell to place token
    **/
   if (type == COMPUTER) {
/*      do { */
         x = rand() % 3;
         y = rand() % 3;
         printf("comp tries %d, %d\n", y, x);
/*      } while (board[y][x] != C_EMPTY); */
      board[y][x] = cur_player->token;
      return SUCCESS;
   }
   else {
      /* human takes their turn */      
      printf("enter coords : ");
      fgets(turn, 3, stdin);
      strtok(turn, ", \n");
      y = turn[0];
      x = turn[2];
      board[y][x] = cur_player->token;
      return SUCCESS;
   }
   return FAILURE;
}
