/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Darrell Thoonen
* Student Number   : s3482232
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "game.h"
#include "player.h"
#include "scoreboard.h"

#ifndef MAIN_H
#define MAIN_H

/**
 * @file main.h contains any data structures or constants you might want to
 * introduce to manage the main menu
 **/

/**
 * menu selection size
 **/

#define SELECTION 1
#define EXTRA_CHAR 2 

enum menu_option {
   /* play game */
   PLAY = 1,
   /* display hi-scores */
   DISPLAY,
   /* exit */
   EXIT
};

typedef enum menu_option menu_select;

#endif



