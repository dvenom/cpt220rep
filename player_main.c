#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "shared.h"
#include "main.h"
#include "player.h"

/*struct player{
   char name [NAMELEN + 1],
   enum player_type type,
   enum cell token
}; */

enum input_result init_human_player (struct player *human, enum cell *token) {
   char player_name [NAMELEN + 1};
   printf("please enter your name\n");
   fgets(player_name, NAMELEN-1, stdin);
   if (sizeof(player_name) != 0) {
      printf("you entered %s\n", player_name);
      human->type = HUMAN;
      human->token = *token;
      return SUCCESS;
   }
   return FAILURE;
}

enum input_result init_computer_player (struct player *computer, enum cell token) {
   computer->name = "Computer";
   computer->type = COMPUTER;
   computer->token = token;
   return SUCCESS;
}

int main(void) {


   /* declare types of players */
   struct player computer, human;

   /* declare token */ 
   enum cell human_token;
   enum cell comp_token; 

  /* int rand_number = rand();

   printf ("token value = %d\n", (rand_number%2)); */

   /* seed the generator */
   srand(time(NULL));  
   if (rand()%2 == 0) {
      human_token = C_NOUGHT;
      comp_token = C_CROSS;
      init_human_player(&human, &human_token);
      init_computer_player(&computer, comp_token);
   }
   else {
      human_token = C_CROSS;
      comp_token = C_NOUGHT;
      init_human_player(&human, &human_token);
      init_computer_player(&computer, comp_token);
   }
   
   printf("%c assigned to human player\n", human.token); 
   printf("%c assigned to the computer player\n", computer.token);

   return EXIT_SUCCESS;
}
