/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Darrell Thoonen
* Student Number   : s3482232
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"
#include "player.h"

/**
 * @file game.c contains the functions required for management of the game
 * while it is being played.
 **/

/**
 * @param human a pointer to the human player struct
 * @param computer a pointer to the computer player struct
 * @return a pointer to the winning player or NULL if there was no
 * winner
 **/
struct player * play_game(struct player * human, struct player * computer)
{
   /**
    * FIRST-TIME THROUGH
    * 1 Determine which player has C_NOUGHT.
    * 2 Assign as current player.
    * LOOP
    * 3 Call take_turn.
    * 4 Call test_for_winner.
    * 5 if) GR_NOWINNER
    * 5.1 Call swap_players
    * 6 else) winner
    * 6.1 Enter into scoreboard
    **/
   
   struct player  *cur_player, *other_player;
   enum cell game_board[BOARDHEIGHT][BOARDWIDTH];
   int cell_occupy_count = 0;
   init_board(game_board);

   if (human->token == C_NOUGHT){
      cur_player = human;
      other_player = computer;
   }
   else {
      cur_player = computer;
      other_player = human;
   }

   for (; cell_occupy_count < 10; ++cell_occupy_count) {
      /* take a turn */
      take_turn(game_board, cur_player);
      /* test if player won */
      switch(test_for_winner(game_board, cur_player->token)) {
         case 0:
         case 1:{
            return cur_player;
         }
         case 2:{
            return NULL;
         }
         case 3:{
            swap_players(&cur_player, &other_player);
         }
      }
      /* if no winner, swap players */
   }
   
        return NULL;
}

/**
 * @param first the first player passed in
 * @param second the second player passed in
 **/
void swap_players(struct player** first, struct player** second)
{
   /**
    * 1 create a temporay player variable
    * 2 move current_player to temp_player
    * 3 move other_player to current_player
    * 4 move temp_player to other_player
    **/

   struct player** temp_player;

   temp_player = first;
   first = second;
   second = temp_player;
}

/**
 * @param board the game_board passed in
 * @param cur_token
 **/
enum game_result test_for_winner(game_board board, enum cell cur_token)
{
   /**
    * 1 get which token is having their turn
    * 2 check contents of height,x (funct checkY)
    * 3 check contents of y,width  (funct checkX)
    * 4 check contents of y,x      (funct checkForwardDiag)
    * 5 check contents of BOARDHEIGHT-y,BOARDWIDTH-x  (funct checkBackDiag)
    * 6 if) any check is true 
    * 6.1 return that token as winner
    * 7 else) return no winner
    **/
    if (check_vert(board, cur_token) || check_horz(board, cur_token) ||
       check_diag(board, cur_token)) {
       if (cur_token == C_NOUGHT){
          printf("%c won\n", (char)cur_token);
          return GR_NOUGHT;
       }
       if (cur_token == C_CROSS){
          printf("%c won\n", (char)cur_token);
          return GR_CROSS;
       }
       if (check_full(board)){
          printf("No-one won");
          return GR_DRAW;
       }
    }
    return GR_NOWINNER;
}

BOOLEAN check_vert(game_board board, enum cell cur_token) {
   int hor, ver = 0;
   int token_count = 0;
   while (ver < BOARDHEIGHT) {
      for (hor = 0;hor < BOARDWIDTH; ++hor) {
       
         if (board[ver][hor] == cur_token){
            ++token_count;
         }
      }
      if (token_count == 3) {
         return TRUE;
      }
      token_count = 0;
      ++ver;
   }
   return FALSE;
}

BOOLEAN check_horz(game_board board, enum cell cur_token) {
   int ver, hor = 0;
   int token_count = 0;
   while (hor < BOARDWIDTH) {
      for (ver = 0; ver < BOARDHEIGHT; ++ver) {
         if (board[ver][hor] == cur_token) {
            ++token_count;
         }
      }
      if (token_count == 3) {
         return TRUE;
      }
      token_count = 0;
      ++hor;
   }
   return FALSE;
}

BOOLEAN check_diag(game_board board, enum cell cur_token) {   
   if((board[0][0] == cur_token) &&
      (board[1][1] == cur_token) &&
      (board[2][2] == cur_token)) {
      return TRUE;
   }

   if((board[0][2] == cur_token) &&
      (board[1][1] == cur_token) &&
      (board[2][0] == cur_token)) {
      return TRUE;
   }

   return FALSE;
}

BOOLEAN check_full(game_board board){
   int ver, hor, cell_occupy_count=0;
   for (ver=0; ver<BOARDHEIGHT; ++ver) {
      for (hor=0; hor<BOARDWIDTH; ++hor) {
         if(board[ver][hor] != C_EMPTY) {
            ++cell_occupy_count;
         }
      }
   }
   if (cell_occupy_count == 9) {
      return TRUE;
   }
   return FALSE;
}
